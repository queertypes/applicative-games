var cb = function() {
    var h = document.getElementsByTagName('head')[0];
    var links = [
        '/css/default.css',
        '/css/syntax.css'
    ];

    links.map (function(href) {
        var l = document.createElement('link');
        l.rel = 'stylesheet';
        l.href = href;
        h.parentNode.insertBefore(l, h);
    });
};

if (requestAnimationFrame) {
    requestAnimationFrame(cb);
} else {
    window.addEventListener('load', cb);
}
