----------
title: Command, Glass, and Honor: Risk of Rain Longevity Games
date: May 17, 2016
tags: risk of rain, games, bugs
----------

[Risk of Rain](http://riskofraingame.com/) is an action, platforming,
rogue-like with beautiful sprites, backgrounds, and music. I fell in
love with this game shortly after its release and continue to play it
to this day to destress.

Frequently, I'll take to playing what I call "sustainable runs". The
rules are simple:

* The game ends when: you die, it crashes, you finish the final boss
* No pausing, even when you go to bed

This post is all about describing some strategies and builds I've
found in order to make these runs successful.

## Character Selection

Everything begins with choosing your character and the artifacts. Choose
the character that you are most comfortable playing as. Since the key
to a sustainable runs is item acquisition, and consequently, initial
artifact selection, the chosen character doesn't matter much.

As for artifacts, you'll need Command, Glass, and Honor.

**Command** makes it so that item drops are no longer
random. You'represented with either a **white** (commons), **green**
(uncommons), **orange** (usables), or **red** (rares) box whenever
enemies would drop loot, when opening chests, or when ? shops are
encountered.

**Glass** multiplies all damages by 5x, but makes player character HP
10%. The value in this is that it makes everything happen
faster. Initial kill speed is greatly accelerated by this (with
enhanced knockback to offset character fragility), but the first 10-15
minutes of play are perilous. Most things can 1-shot you.

**Honor** makes all enemies spawn as elites. This includes bosses. Why
would this play a role in making a sustainable run? It serves as an
accelerator when coupled with
[56-leaf clover](http://riskofrain.wikia.com/wiki/56_Leaf_Clover).

Acquiring those artifacts:

* [Command](http://riskofrain.wikia.com/wiki/Command)
* [Glass](http://riskofrain.wikia.com/wiki/Glass)
* [Honor](http://riskofrain.wikia.com/wiki/Honor)

With that, let's talk about a few key items.

## Crucial Items

Items. Items. Items. These are what make sustainable runs possible,
entertaining, and varied. There's more than one way to get there, but
a few items have made their way to my builds every time. This is a
list of said items with some commentary.

* [Guardian's Heart](http://riskofrain.wikia.com/wiki/Guardian's_Heart):
  adds 60 "energy shield" per heart acquired. This is really important
  early on, because if an attack would 1-shot you, it now just empties
  your shield. It also conveniently refills within seconds.

* [56-Leaf Clover](http://riskofrain.wikia.com/wiki/56_Leaf_Clover):
  The key to acquiring most of your items quickly. Each one acquired
  gives elite monster a +1% chance to drop an item. At 100, this means
  every monster you slay will drop **something**.

* [Infusion](http://riskofrain.wikia.com/wiki/Infusion): Increases
  your max HP by 1 for each kill. This is crucial for end-game
  sustainability. Stacks at +0.5 HP/kill at each additional one.

* [Paul's Goat Hoof](http://riskofrain.wikia.com/wiki/Paul%27s_Goat_Hoof):
  Gotta go fast. Makes it easier to dodge everything and to make
  progress early on. Speed seems to stop increasing after about 20
  hooves.

* [Crowbar](http://riskofrain.wikia.com/wiki/Crowbar): Grants a
  significant damage bonus to mostly (>95% HP?) undamaged
  enemies. This makes your damage scale nicely. Get enough crowbars,
  and eerything falls on the first hit. Handy for while you're
  actively playing the game.

* [Bitter Root](http://riskofrain.wikia.com/wiki/Bitter_Root):
  Increases Max HP multiplicatively. Coupled with Infusion, helps your
  HP cap out at 9999 as soon as possible.

* [Bustling Fungus](http://riskofrain.wikia.com/wiki/Bustling_Fungus):
  The most powerful recovery item in the game... if you can get 2
  seconds of rest. Heals 90% of your MAX HP per second when you've
  gathered 20 of them. Finding time to rest is surprisingly easy after
  the first 10 minutes of gameplay.

* [Lens-Maker's Glasses](http://riskofrain.wikia.com/wiki/Lens-Maker%27s_Glasses):
  Enables critical hits. Stacks up to 100% chance. Critical hits are
  nice. More damage scaling.

* [Bundle of Fireworks](http://riskofrain.wikia.com/wiki/Bundle_of_Fireworks):
  On opening a chest, making a purchase, or repairing a drone,
  launches homing, long-range missiles. Very handy for early game. 1
  is plenty. 5 is amusing. 25 or more will probably lag your game for
  minutes. I crashed the game at 300 stacks, once...

* [Smart Shopper](http://riskofrain.wikia.com/wiki/Smart_Shopper):
  sharply increases worth of enemy coin drops. Very important in the
  early-game.

* [Rusty Jetpack](http://riskofrain.wikia.com/wiki/Rusty_Jetpack):
  Makes your jumps go further. Seems to stack infinitely so you can
  jump waaaaay out there.

* [Frost Relic](http://riskofrain.wikia.com/wiki/Frost_Relic): Potent,
  short-range ice barrier is created whenever you kill an
  enemy. Stacks infinitely, and can make the game lag if you've
  acquired more than say... 120?

* [Hopoo's Feather](http://riskofrain.wikia.com/wiki/Hopoo_Feather):
  Double jump! And it stacks!!!

* [Ceremonial Dagger](http://riskofrain.wikia.com/wiki/Ceremonial_Dagger):
  Launches several homing particles on killing an enemy. This should
  be the first rare you acquire when going for a sustainability
  run. Makes the early game comfortable.

There are many, many more items I've not mentioned. These form a sort
of core to get things going. However, we're going for something we can
sleep on.

## Early Game

This is the first half hour of game play. The priorities are, in
suggested order:

* [Skeleton Key](http://riskofrain.wikia.com/wiki/Skeleton_Key) - open all treasure chests
* Guardian's hearts (1-2): to survive more than 10 minutes
* ceremonial daggers (1-10): to speed up early killing
* goat's hooves (10x): to make things go faster
* smart shopper (1): to afford all drones and/or boxes
* infusion (1): more HP
* bitter roots (5-10): more HP
* 56-leaf clover: start drops rolling

The early game is all about staying alive and getting the ball
rolling. I build some HP here and make it possible to gather as many
chests and drones as possible.

## Mid Game

This is the next two hours, and sets the tone for the rest of the
run. The priorities are, in suggested order:

* Keep Skeleton Key as your use-item
* Mobility: feathers (up to 2), rusty jetpacks (up to 2)
* 56-leaf clovers: get as close to 100 as your patience can manage - 100 is ideal
* Recovery: get enough bitter mushroom (10-20) for comfy recovery speed
* Damage: bring on the crowbars (20-50 for now)
* Choose your build: start acquiring items you need for your main build

The feel of this stage is more relaxing. You'll quickly get to the
point where you're unkillable while running around, and most enemies
will die with a single attack. The tables turn here compared to the
early game.

## End Game

* Max out those clovers: every enemy should drop an item
* Swap the use-item for something good for your build
* Max out HP
* Boost your passive recovery: should be recovering 300+HP/second
* Focus on your build

This is where final touches and some observation come into
play. You'll need to gather enough items to be able to leave your game
running without dying. This takes some trial and error, and the
strategies even vary by stage. It's probably easiest to test with the
Ancient Ruins stage, since the bosses there are some of the
weakest. You'll want to avoid Overloaded Magma Warms and Cremators for
a first run, as these are some of the hardest to survive while idling.

Another note - enemies will drop items. **A lot** of items. It can
help to spend 2 or 3 minutes just killing things, then stopping to
pick up big stacks of things, rather than picking things up on
drop. This will be especially true after you've acquired 100 56-Leaf
Clovers.

## Example Build: Missile Factory Factory

Here's a build I used to good effect over the past two days. It
focuses on acquiring lots and lots of drones (40+) and then maximizing
Frost Relic and
[Arms Race](http://riskofrain.wikia.com/wiki/Arms_Race). If any enemy
gets within range of you, they should die **very quickly**.

Here's the final outcome:

![](/images/ror-missile-factory-1.png){width=50% height=50%}

Play-style: run into enemies and let drones explode them followed by
ice barrier shards.

Idle-style: choose a nice flat area and let the enemies come to you.

Another screenshot:

![](/images/ror-missile-factory-2.png){width=50% height=50%}

## Additional Tips

* [Dio's Best Friend](http://riskofrain.wikia.com/wiki/Dio%27s_Best_Friend):
  Revives you on death. It's a good idea to gather at least a few of
  these before going to sleep in a new area. If your character is
  dying once per hour, 8-10 of these will make for a good night's
  rest, where you can collect loot and make some tweaks in the
  morning.

* Overloaded Magma Worms are the worst
* Cremators are the second worst, and often more annoying because they
  resist most idle play strategies.
* Scavengers start to get dangerous several hours in
* Almost all enemies are (elite) bosses after 6 hours

## Closing

Risk of Rain can get silly sometimes. It encourages tinkering with the
vast array of items to find something that works for you. With this
guide, I've just laid out some of the key components of one of the
most relaxing ways for me to enjoy this game.

Thanks for reading!
