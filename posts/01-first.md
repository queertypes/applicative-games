-----
title: Hi! Welcome!
date: February 8, 2015
tags: haskell, links
-----

Welcome to Applicative Game Development! Be sure to check out the
[about](/about.html) for what this is all about.

To open things up, I'd like to take this post to point to several
other resources. I'll be using many of these as I write and share my
experiences in game development.

If you know of a library or resource that you'd like to see added to
this page, feel free to reach out to me
[here](https://queertypes.com/contact.html).

## Haskell

Many others have curated Haskell programming resources. Check out any
of these to get your environment set up or to familiarize yourself
with the language I'll be using.

* [Learn Haskell](https://github.com/bitemyapp/learnhaskell)
* HaskellNow [Learning](http://www.haskellnow.org/wiki/LearningResources) and [Resources](http://www.haskellnow.org/wiki/Reading)
* [What I Wish I Knew When Learning Haskell](http://dev.stephendiehl.com/hask/)
* [Cabal Guide](http://katychuang.com/cabal-guide/)

## Game Development in Haskell

Diving into the more specific, what have others written about Haskell
game development?

### Reading

* [Game Programming in Haskell](https://leanpub.com/gameinhaskell)
* [HaskellWiki: Game Development](https://wiki.haskell.org/Game_Development)

### A Few Games

* [Frag FPS Game](https://wiki.haskell.org/Frag)
* [Nikki and the Robots](https://github.com/nikki-and-the-robots/nikki)
* [Wayward Tide](http://blog.chucklefish.org/?p=154)

### Libraries

Below is a sampling of libraries that assist with input-handling and
rendering:

* [SDL2](https://hackage.haskell.org/package/sdl2)
* [GLFW-b](http://hackage.haskell.org/package/GLFW-b)
* [Helm](http://helm-engine.org/)
* [OpenGL](https://hackage.haskell.org/package/OpenGL)
* [JuicyPixels](https://hackage.haskell.org/package/JuicyPixels)
* [Diagrams](http://projects.haskell.org/diagrams/)
* [OpenAL](https://hackage.haskell.org/package/OpenAL)
* [ALUT](https://hackage.haskell.org/package/ALUT)

## Functional Reactive Programming

Of particular interest to me, is understanding how functional reactive
programming (FRP) can be incorporated into a game engine
efficiently. Towards this end, I'll be diving into existing code,
libraries, and papers to see how this might be achieved, and where the
tricky bits lie.

## Next Time

With this introduction complete, in upcoming entries, I hope to show you all how to:

* create and manage a window
* handle keyboard entry: move, quit
* render a cute block creature: as OpenGL rectangles and with image assets
* using both [SDL2](https://hackage.haskell.org/package/sdl2) and [GLFW-b](http://hackage.haskell.org/package/GLFW-b)
* add sound effects

Once that introductory series is complete, we can move on to something
more ambituous.

'til next time!
