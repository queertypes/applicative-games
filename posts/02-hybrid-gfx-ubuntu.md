-----
title: Getting NVIDIA/Intel to Work Together on Ubuntu
date: February 23, 2015
tags: ubuntu, nvidia, intel
-----

A working graphics environment is pretty important for video game
development. I was lacking such an environment for the past two
weeks. As of today, that's been fixed.

It's been no easy feat. At the end of this guide, I hope to save you
all some pain by sharing what worked for me.

What works, by the end of this:

* Intel HD graphics for most things
* With `optirun` or `primusrun`: NVIDIA graphics
* Settings/modules persist across reboots

What might not work:

* Hibernate/suspend: required a reboot for me
    * NVIDIA card disappeared off of PCI listing for that session

What I don't cover:

* Multi-monitor set ups
    * I develop on a laptop w/o external displays, so...

Everything given below assumes a fresh [*]Ubuntu install. I imagine
instructions similar to these could work for other Linux distrbutions.

I also assume you have hardware that consists of an NVIDIA discrete
card and an Intel integrated GPU, which is the case for me.

## Instructions

Here's the stream of instructions. See in the appendix sections below
for changes needed to:

* /etc/X11/xorg.conf
* /etc/bumblebee/bumblebee.conf
* /etc/bumblebee/xorg.conf.nvidia
* /etc/init/gpu-manager.conf

I've linked most of the resources I used as I got things working. Be
warned, many things can go wrong. Most of the time, I encountered a
black screen on TTY-7 when things weren't quite working. Check out the
**reset** section below for another set of commands to get you back to
square one in case something doesn't quite work.

```bash
sudo apt-get update

# to install the command `apt-add-repository`
sudo apt-get install python-software-properties

# to subscribe to a PPA that has the latest NVIDIA driver available
sudo apt-add-repository ppa:xorg-edgers/ppa
sudo apt-get update

# remove open source driver nouveau, to avoid conflicts
#  * might not be installed; that's okay -
#    just make sure it isn't there
sudo apt-get remove xserver-xorg-video-nouveau

# for optirun and hybrid graphics
sudo apt-get install\
     nvidia-346 bumblebee primus\    # nvidia, optirun
     libgl1-mesa-dri libgl1-mesa-glx # CPU OpenGL
     libgl1-mesa-dri:i386            # 32-bit GL for Steam
     libgl1-mesa-glx:i386              and 32-bit game binaries
     mesa-utils                      # glxinfo testing

# to set Intel driver as main, but still load NVIDIA driver
sudo nano /etc/X11/xorg.conf

# to disable gpu manager, which overwrites xorg.conf on boot
sudo nano /etc/init/gpu-manager.conf

# to configure hybrid graphics
sudo nano /etc/bumblebee/bumblebee.conf
sudo nano /etc/bumblebee/xorg.conf.nvidia

# to select GL libs corresponding to Intel CPU; select mesa
sudo update-alternatives --config x86_64-linux-gnu_gl_conf
sudo ldconfig

# write changes to filesystem (just in case); reboot
sync
sudo reboot now
```

At this point, try running the following commands:

```bash
# CPU-side; your renderer may vary
$ glxinfo | grep 'direct rendering'
direct rendering: Yes
$ glxinfo | grep 'OpenGL renderer string'
OpenGL renderer string: Mesa DRI Intel(R) Haswell Mobile

# GPU-side; your renderer may vary
$ optirun glxinfo | grep 'direct rendering'
direct rendering: Yes
$ optirun glxinfo | grep 'OpenGL renderer string'
OpenGL renderer string: GeForce GTX 860M/PCIe/SSE2
```

If the output looks good, there's a high chance everything is working.

## Reset: Something Didn't Work

These commands will take your computer back to the point where you at
least have CPU rendering working, and therefore can run a desktop
environment to debug further.

```bash
# get rid of everything NVIDIA-related
sudo apt-get purge nvidia*

# remove the xorg.conf file; X11 works w/o it as CPU-only
sudo rm /etc/X11/xorg.conf;

# reset installs
sudo apt-get install --reinstall\
     xserver-xorg-core\                           # X11 core libs
     libgl1-mesa-glx:i386 libgl1-mesa-dri:i386\   # 32-bit GL
     libgl1-mesa-glx:amd64 libgl1-mesa-dri:amd64  # 64-bit GL

# reset configuration
sudo dpkg-reconfigure xserver-xorg
```

---

## Appendix - /etc/X11/xorg.conf: X11 Settings

```xorg
# The following may need adjusting:
#  Device:intel:BusID
#  Device:nvidia:BusID
# Find that information by running lspci
#    01:00.0  3D Controller: NVIDIA...
# -> BusID "PCI:1@0:0:0"
Section "ServerLayout"
  Identifier "layout"
  Screen 0 "intel"
  Inactive "nvidia"
EndSection

Section "Device"
  Identifier "intel"
  Driver "intel"
  BusID "PCI:0@0:2:0"
  Option "AccelMethod" "SNA"
EndSection

Section "Screen"
  Identifier "intel"
  Device "intel"
EndSection

Section "Device"
  Identifier "nvidia"
  Driver "nvidia"
  BusID "PCI:1@0:0:0"
  Option "ConstrainCursor" "off"
EndSection

Section "Screen"
  Identifier "nvidia"
  Device "nvidia"
  Option "AllowEmptyInitialConfiguration" "on"
  Option "IgnoreDisplayDevices" "CRT"
EndSection
```

## Appendix - /etc/init/gpu-manager.conf: GPU Manager Settings

```bash
# Comment these out; GPU Manager ruins our work
#start on (starting lightdm
#          or starting kdm
#          or starting xdm
#          or starting lxdm)
task
exec gpu-manager --log /var/log/gpu-manager.log
```

## Appendix - /etc/bumblebee/bumblee.conf: Bumblebee Settings

```ini
# These paths work for me. The following may need adjusting:
#   optirun:PrimusLibraryPath
#   driver-nvidia:KernelDriver
#   driver-nvidia:LibraryPath
#   driver-nvidia:XorgModulePath
[bumblebeed]
VirtualDisplay=:8
KeepUnusedXServer=false
ServerGroup=bumblebee
TurnCardOffAtExit=false
NoEcoModeOverride=false
Driver=nvidia
XorgConfDir=/etc/bumblebee/xorg.conf.d

[optirun]
Bridge=auto
VGLTransport=proxy
PrimusLibraryPath=/usr/lib/x86_64-linux-gnu/primus:/usr/lib/i386-linux-gnu/primus
AllowFallbackToIGC=false

[driver-nvidia]
KernelDriver=nvidia_346
PMMethod=auto
LibraryPath=/usr/lib/nvidia-346:/usr/lib32/nvidia-346
XorgModulePath=/usr/lib/nvidia-346/xorg,/usr/lib/xorg/modules
XorgConfFile=/etc/bumblebee/xorg.conf.nvidia

# driver-nouveau omitted
```

## Appendix - /etc/bumblebee/xorg.conf.nvidia: Bumblebee Settings

```xorg
Section "ServerLayout"
  Identifier  "Layout0"
  Option      "AutoAddDevices" "false"
  Option      "AutoAddGPU" "false"
EndSection

Section "Device"
  Identifier  "DiscreteNvidia"
  Driver      "nvidia"
  VendorName  "NVIDIA Corporation"
  BusID       "PCI:1@0:0:0"  # lspci for this
  Option "NoLogo" "true"
  Option "UseEDID" "false"
  Option "UseDisplayDevice" "none"
EndSection
```

## Appendix - Update Alternatives: GL on CPU by Default

```bash
$ sudo update-alternatives --config x86_64-linux-gnu_gl_conf

There are 3 choices for the alternative
  x86_64-linux-gnu_gl_conf
(providing /etc/ld.so.conf.d/x86_64-linux-gnu_GL.conf).

Selection    Path                                       Priority   Status
------------------------------------------------------------
0            /usr/lib/nvidia-346/ld.so.conf              8604      auto mode
1            /usr/lib/nvidia-346-prime/ld.so.conf        8603      manual mode
2            /usr/lib/nvidia-346/ld.so.conf              8604      manual mode
* 3          /usr/lib/x86_64-linux-gnu/mesa/ld.so.conf   500       manual mode

Press enter to keep the current choice[*], or type selection number: 3
```

## Appendix - Useful Commands and Logs

These helped me figure out what was going on:

* `lspci`: lists all devices connected on the PCI bus
    * `lspci -nnk`: verbose
* `dmesg`: kernel log; are things even compatible? crashing? detected?
* `more /var/log/Xorg.0.log`: view X11 server logs; did modules load correctly?
* `uname -r`: what kernel version am I running?
* `lsmod`: what modules are loaded?
    * `modinfo nvidia`: module info for NVIDIA driver
        * NVIDIA driver may have a different name
        * tab-complete `modinfo nvi` to find out what name it goes by
* `more /proc/driver/nvidia/version`: another way to look at NVIDIA driver details
* `more /proc/driver/nvidia/gpus/<id>/information`: hardware info
* `sudo service lightdm restart`: force display manager to reboot
    * Depending on Linux distro, the display manager and the command may vary
* `Ctrl-Alt-F1`: switch to TTY 1, that doesn't depend on desktop env
    * It's a nice save when all you've got is a black screen
    * `Ctrl-Alt-F7` takes you back to the default desktop TTY (7)

---

## Resources

1. Arch Wiki, [NVIDIA](https://wiki.archlinux.org/index.php/NVIDIA)
2. Ubuntu Wiki, [NVIDIA](https://help.ubuntu.com/community/BinaryDriverHowto/Nvidia)
3. Ubuntu Forums, [GPU Manager Issue](http://ubuntuforums.org/showthread.php?t=2220552)
4. Ask Ubuntu, [lspci](http://askubuntu.com/questions/285647/bumblebee-issue-with-13-04) to find BusID
5. Xorg Edgers, [PPA](https://launchpad.net/~xorg-edgers/+archive/ubuntu/ppa)
6. Steam Community, [Optimus on Linux](https://support.steampowered.com/kb_article.php?ref=6316-GJKC-7437)
