---
title: About
---

Welcome to Applicative Game Development!

This blog is all about game development with functional
techniques. Functional reactive programming, event handling, OpenGL,
type systems, Haskell, and asset management will all make an
appearance. Introductory topics, like choosing libraries,
input-handling styles, and getting that first window going will be
covered. Rendering, game loop management, optimization, typeful
techniques, and more will eventually feature.

Why purely functional techniques? In large part, it's because I love
working with programming languages like Haskell. It's a lot of fun for
me. More practically, I think there's interesting ideas to discover in
this domain, and I'd like to see what works and what doesn't.

Ultimately, whatever the outcome, I hope to make this one more
resource on how to make games using purely functional techniques.

```haskell
module Main where

import Control.Concurrent
import Control.Monad
import App.Main (withWindow, render)

data Topics
  = Rendering
  | TypeSystems
  | WindowManagement
  | Haskell
  | FunctionalReactiveProgramming
  | AssetManagement
  | GameLoop
  | OpenGL
  | EventHandling
  deriving Show

keyHandler :: Window -> Key -> Int -> KeyState -> ModifierKeys -> IO ()
keyHandler _ k _ _ _ = print ("Press " ++ show k ++ " to play")

loop :: Window -> IO ()
loop win = go win 0 60
  where go :: Window -> Int -> Int -> IO ()
        go w fps fpsCap
          | fps > fpsCap = slowDown w fps fpsCap
          | otherwise    = step w fps fpsCap

        slowDown w fps fpsCap = threadDelay 50000 >> go w (fps-2) fpsCap
        step     w fps fpsCap = do
          k <- getKey w Key'Escape
          case k of
            KeyState'Pressed -> return ()
            _                -> pollEvents >> go w (fps+1) fpsCap

main :: IO ()
main = do
  putStrLn "Let's make some games!"
  withWindow 640 480 "Applicative Game Development" keyHandler loop
```
