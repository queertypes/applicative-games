--------------------------------------------------------------------------------
{-# LANGUAGE OverloadedStrings #-}
import Control.Applicative
import Data.Monoid (mconcat, (<>))
import Hakyll

feedConf :: FeedConfiguration
feedConf = FeedConfiguration
    { feedTitle = "Applicative Game Development"
    , feedDescription = "Game development with purely functional techniques."
    , feedAuthorName = "Allele Dev"
    , feedAuthorEmail = "allele.dev@gmail.com"
    , feedRoot = "https://applicative-games.queertypes.com"
    }

--------------------------------------------------------------------------------
main :: IO ()
main = hakyll $ do
    tags <- buildTags "posts/*" $ fromCapture "tags/*.html"
    let assets = ["images/*", "files/*"]
        templatize = loadAndApplyTemplate

    match (foldr1 (.||.) assets) $ route idRoute >> compile copyFileCompiler
    match "css/*" $ route idRoute >> compile compressCssCompiler
    match "js/*" $ route idRoute >> compile copyFileCompiler

    match (fromList ["about.md", "projects.md"]) $
      route (setExtension "html") >>
      compile (pandocCompiler >>=
               templatize "templates/default.html" defaultContext >>=
               relativizeUrls)

    match "posts/*" $
        route (setExtension "html") >>
        compile (pandocCompiler >>=
                 saveSnapshot "content" >>=
                 templatize "templates/post.html" (postCtx tags) >>=
                 templatize "templates/default.html" defaultContext >>=
                 relativizeUrls)

    match "index.html" $ do
        route idRoute
        compile $ do
            posts <- recentFirst =<< loadAll "posts/*"
            let ctx = indexCtx tags posts
            getResourceBody
                >>= applyAsTemplate ctx
                >>= templatize "templates/default.html" defaultContext
                >>= relativizeUrls

    tagsRules tags $ \tag pattern -> do
        let title = "Posts tagged: " <> tag
        route idRoute
        compile $ do
            posts <- loadAll pattern
            let ctx = constField "title" title <>
                      listField "posts" (postCtx tags) (return posts) <>
                      defaultContext

            makeItem ""
                 >>= templatize "templates/posts.html" ctx
                 >>= templatize "templates/default.html" defaultContext
                 >>= relativizeUrls

    match "templates/*" $ compile templateCompiler

    let feedCtx = postCtx tags <> bodyField "description"
        ps = loadAllSnapshots "posts/*" "content"
        recent n = fmap (take n) . recentFirst
        createFeed path f = create [path] $ route idRoute >> compile f

    createFeed "atom.xml" (ps >>= recent 10 >>= renderAtom feedConf feedCtx)
    createFeed "rss.xml" (ps >>= recent 10 >>= renderRss feedConf feedCtx)
    createFeed "atom-haskell.xml" (taggedPosts tags "haskell" <$> ps >>=
                                   recent 10 >>= renderAtom feedConf feedCtx)
    createFeed "rss-haskell.xml" (taggedPosts tags "haskell" <$> ps >>=
                                  recent 10 >>= renderRss feedConf feedCtx)

--------------------------------------------------------------------------------
taggedPosts :: Tags -> String -> [Item String] -> [Item String]
taggedPosts tags tag posts =
  let tagged = lookup tag $ tagsMap tags
      idIn = elem . itemIdentifier
      filterByTag (Just ts) ps = filter (`idIn` ts) ps
      filterByTag Nothing _ = []
  in
      filterByTag tagged posts

indexCtx :: Tags -> [Item String] -> Context String
indexCtx tags posts = mconcat
  [ listField "posts" (postCtx tags) (return posts)
  , constField "title" "Home"
  , field "tags" (\_ -> renderTagList tags)
  , defaultContext
  ]

postCtx :: Tags -> Context String
postCtx tags = mconcat
    [ dateField "date" "%B %e, %Y"
    , tagsField "tags" tags
    , defaultContext
    ]
